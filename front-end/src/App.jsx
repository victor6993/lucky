import React, {useState, useEffect} from 'react';
import './App.scss';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import {Redirect} from "react-router";
import Access from "./pages/Access/Access";
import Home from "./pages/Home/Home";
import Onboarding from "./pages/Onboarding/Onboarding";
import Footer from "./shared/components/c-footer/Footer";
import UserProfile from "./pages/UserProfile/UserProfile";
import More from "./pages/More/More";
import AnimalProfile from "./pages/AnimalProfile/AnimalProfile";
import FormAdoption from "./pages/FormAdoption/FormAdoption";
import AdoptionState from "./pages/AdoptionState/AdoptionState";
import UserContext from "./shared/contexts/UserContext";
import CloudinaryPrueba from "./pages/CloudinaryPrueba/CloudinaryPrueba";
import firebase from "firebase"
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth"
import environment from "./environment/environment";
import Login from "./pages/Login/Login";
import axios from "axios";
import Maps from "./pages/Maps/Maps";
import Adoption from './pages/Adoption/Adoption';
import AdoptionDetails from "./pages/AdoptionDetails/AdoptionDetails";

firebase.initializeApp({
    apiKey: environment.firebaseApiKey,
    authDomain: environment.firebaseAuthDomain
});


function App() {

    const [user, setUser] = useState({});
    const [loggedIn, setLoggedIn] = useState(false);
    const [isValidToken, setIsValidToken] = useState(null);


    useEffect(() => {
        setUser(JSON.parse(localStorage.getItem('lucky-user')));
        firebase.auth().onAuthStateChanged(user => {
            setLoggedIn(!!user);
        });
    }, []);


    useEffect(() => {
        const headers = {authorization: localStorage.getItem('lucky-token')}
        axios.post('http://localhost:3002/verify', {}, {headers: headers}).then(res => {
            res.status < 400 ? setIsValidToken(true) : setIsValidToken(false);
        }).catch(err => {
            setIsValidToken(false);
            console.log(err);
        });
    }, [isValidToken]);


    const getUser = (loggedUser) => {
        setUser(loggedUser);
        setIsValidToken(true);
    }

    const uiConfig = {
        signInFlow: "popup",
        signInOptions: [
            firebase.auth.GoogleAuthProvider.PROVIDER_ID,
            firebase.auth.FacebookAuthProvider.PROVIDER_ID,
            firebase.auth.TwitterAuthProvider.PROVIDER_ID,
            firebase.auth.GithubAuthProvider.PROVIDER_ID
        ],
        callback: {
            signInSuccess: () => false
        }
    }

    const firebaseLogIn = loggedIn ? <Redirect to="/home"/> :
        <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()}/>;

    return (
        <div className="container-fluid">
            <Router>
                <UserContext.Provider value={[user, setUser]}>
                    <Switch>
                        {isValidToken !== null && (
                            <>
                                <Route path="/cloudinary">
                                    <CloudinaryPrueba/>
                                </Route>

                                <Route path="/adoption-status">
                                    {loggedIn || isValidToken ? <AdoptionState/> : <Redirect to="/access"/>}
                                </Route>

                                <Route path="/adoption-details/:id/:adoptionId">
                                    {loggedIn || isValidToken ? <AdoptionDetails/> : <Redirect to="/access"/>}
                                </Route>

                                <Route path="/adoption">
                                    {loggedIn || isValidToken ? <> <Adoption/> <Footer/> </> : <Redirect to="/access"/>}
                                </Route>

                                <Route path="/maps">
                                    {loggedIn || isValidToken ? <> <Maps/> <Footer/> </> : <Redirect to="/access"/>}
                                </Route>

                                <Route path="/animalprofile/:id">
                                    {loggedIn || isValidToken ? <AnimalProfile/> : <Redirect to="/access"/>}
                                </Route>

                                <Route path="/userprofile">
                                    {loggedIn || isValidToken ? <> <UserProfile/> <Footer/> </> :
                                        <Redirect to="/access"/>}
                                </Route>

                                <Route path="/more">
                                    {loggedIn || isValidToken ? <> <More/> <Footer/> </> : <Redirect to="/access"/>}
                                </Route>

                                <Route path="/formadoption">
                                    {loggedIn || isValidToken ? <FormAdoption/> : <Redirect to="/access"/>}
                                </Route>

                                <Route path="/home">
                                    {loggedIn || isValidToken ? <> <Home/> <Footer/> </> :
                                        <Redirect to="/access"/>}
                                </Route>

                                <Route path="/access">
                                    <Access/>
                                </Route>

                                <Route path="/login">
                                    <Login
                                        getUser={getUser}
                                        logInFirebase={firebaseLogIn}
                                    />
                                </Route>

                                <Route exact path="/">
                                    {loggedIn || isValidToken ? <> <Redirect to="/home"/> </> : <Onboarding/>}
                                </Route>
                            </>)}
                    </Switch>
                </UserContext.Provider>
            </Router>
        </div>
    );
}

export default App;
