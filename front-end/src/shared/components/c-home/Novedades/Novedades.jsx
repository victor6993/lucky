import React from "react";
import './Novedades.scss'

function Novedades() {

    const dataNovedades = [
        {
            img: process.env.PUBLIC_URL+'assets/images/chinchillas.png',
            title: "10 Curiosidades sobre las chinchillas",
        },
        {
            img: process.env.PUBLIC_URL+'assets/images/iguana.png',
            title: "¿Sabes qué comen las iguanas?",
        },
        {
            img: process.env.PUBLIC_URL+'assets/images/perro.png',
            title: "10 lugares para visitar con tu perro en Madrid",
        }
    ]

    return(
        <section className="c-novedades">
            <p className="c-novedades__section-title">Novedades</p>
            {dataNovedades.map((item, index) =>
              <div key={index} className="c-novedades__item-box">
                  <img className="c-novedades__img" src={item.img} alt={item.title}/>
                  <p className="c-novedades__title">{item.title}</p>
              </div>
            )}
        </section>
    );
}

export default Novedades;