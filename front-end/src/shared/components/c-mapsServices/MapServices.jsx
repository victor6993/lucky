import React, {useContext, useState} from 'react';
import './MapServices.scss';
import axios from 'axios';
import {useForm} from "react-hook-form";
import UserContext from "../../contexts/UserContext";


function MapServices(props) {

    const {register, handleSubmit} = useForm();
    const [showServiceDetail, setShowServiceDetail] = useState(false);
    const [serviceDetail, setServiceDetail] = useState({});
    const [servicePunctuation, setServicePunctuation] = useState(0);
    const [expanded, setExpanded] = useState(false);
    const [punctuationValue, setPunctuationValue] = useState(0);
    const [user, setUser] = useContext(UserContext);

    const showServices = async (id) => {

        try {
            await axios.get('http://localhost:3002/service/' + id).then(res => {
                setServiceDetail(res.data);
                let punctuation = 0;
                for (const opinion of res.data.opinions) {
                    punctuation += opinion.valoration;
                }
                punctuation = punctuation / res.data.opinions.length;
                isNaN(punctuation) ? punctuation = "" : punctuation = punctuation;
                setServicePunctuation(punctuation);
            }).catch(err => console.log(err)).finally(() => setShowServiceDetail(true));
        } catch (err) {
            console.log(err);
        }
    }

    const addOpinion = (data) => {
        data['userId'] = user._id;
        data['serviceId'] = serviceDetail._id;
        data['valoration'] = punctuationValue;
        axios.post('http://localhost:3002/opinion', {data}).then(res => {
            console.log(res);
        }).catch(err => console.log(err)).finally(showServices);
    }

    const arrowOpinions = () => {
        setExpanded(expanded ? false : true);
    }

    const valorate = (num) => {
        setPunctuationValue(num);
    }


    return (
        <>
            <div className="c-more-options">
                {!showServiceDetail &&
                <div className="c-more-options__container1 c-more-options__container1--map-services">
                    {props.services.map((item, index) =>
                        <p key={index} className="c-more-options__item c-more-options__item--map-services"
                           onClick={() => showServices(item.id)}>
                            <img className="c-more-options__img c-more-options__img--map-services"
                                 src={process.env.PUBLIC_URL + "/assets/img/veterinario.png"}
                                 alt="protectora"/>
                            <span className="c-more-options__text">{item.name}</span>
                            <img className="c-more-options__arrow c-more-options__arrow--map-services"
                                 src={process.env.PUBLIC_URL + "/assets/img/arrow.png"}
                                 alt="arrow_bottom_pink"/>
                        </p>
                    )}
                </div>}

                {serviceDetail && showServiceDetail && <div className="c-map-service">
                    <p className="c-map-service__title">{serviceDetail.name}</p>
                    <img className="c-map-service__arrow-top" onClick={() => setShowServiceDetail(false)}
                         src={process.env.PUBLIC_URL + "/assets/images/arrowTop@3x.png"} alt="arrowTop"/>
                    <div className="c-map-service__data-box">
                        {serviceDetail.imageUrl &&
                        <img className="c-map-service__service-img" src={serviceDetail.imageUrl}
                             alt="serviceImage"/>}
                        <div className="c-map-service__service-data">
                            <p className="c-map-service__title  c-map-service__title--punctuation">Puntuación</p>
                            <div className="c-map-service__punctuation-box">
                                <span
                                    className="c-map-service__title c-map-service__title--punctuation-number">{servicePunctuation}</span>
                                <div className="c-map-service__score">
                                    <img className="c-map-service__score-img" onClick={() => valorate(1)}
                                         src={punctuationValue >= 1 ? process.env.PUBLIC_URL + "/assets/ICON/patitas_rojo.png" : process.env.PUBLIC_URL + "/assets/ICON/patitas@3x.png"}
                                         alt="patitas"/>
                                    <img className="c-map-service__score-img" onClick={() => valorate(2)}
                                         src={punctuationValue >= 2 ? process.env.PUBLIC_URL + "/assets/ICON/patitas_rojo.png" : process.env.PUBLIC_URL + "/assets/ICON/patitas@3x.png"}
                                         alt="patitas"/>
                                    <img className="c-map-service__score-img" onClick={() => valorate(3)}
                                         src={punctuationValue >= 3 ? process.env.PUBLIC_URL + "/assets/ICON/patitas_rojo.png" : process.env.PUBLIC_URL + "/assets/ICON/patitas@3x.png"}
                                         alt="patitas"/>
                                    <img className="c-map-service__score-img" onClick={() => valorate(4)}
                                         src={punctuationValue >= 4 ? process.env.PUBLIC_URL + "/assets/ICON/patitas_rojo.png" : process.env.PUBLIC_URL + "/assets/ICON/patitas@3x.png"}
                                         alt="patitas"/>
                                    <img className="c-map-service__score-img" onClick={() => valorate(5)}
                                         src={punctuationValue >= 5 ? process.env.PUBLIC_URL + "/assets/ICON/patitas_rojo.png" : process.env.PUBLIC_URL + "/assets/ICON/patitas@3x.png"}
                                         alt="patitas"/>
                                </div>
                            </div>
                            <p className="c-map-service__street">{serviceDetail.street}</p>
                            <p className="c-map-service__city">{serviceDetail.zipCode} {serviceDetail.city}</p>
                        </div>
                    </div>
                    <p className="c-map-service__title">
                        Opiniones
                        <img style={expanded ? {transform: "rotate(0deg)"} : {transform: "rotate(180deg)"}}
                             onClick={arrowOpinions}
                             className="c-map-service__arrow-top c-map-service__arrow-top--opinion"
                             src={process.env.PUBLIC_URL + "/assets/images/arrowTop@3x.png"} alt="arrowTop"/>
                    </p>
                    {expanded && <div className="c-map-service__opinion-box">
                        {serviceDetail.opinions.map(item =>
                            <div>
                                <img className="c-map-service__user-img" src={item.userImage} alt="userImage"/>
                                <p className="c-map-service__text">
                                    "{item.text}"
                                </p>
                            </div>)}
                    </div>}
                    <p className="c-map-service__title">¿Has estado alguna vez aquí?</p>
                    <form onSubmit={handleSubmit(addOpinion)}>
                        <textarea className="c-map-service__opinion-text" name="text" cols="38" rows="5"
                                  placeholder="Cuéntanos tu experiencia" ref={register} required={true}></textarea>
                        <button className="c-map-service__button">Enviar</button>
                    </form>
                </div>}
            </div>
        </>
    );
}

export default MapServices;