import React, {useState} from 'react';
import "./AdoptionFilter.scss"
import {Dialog} from 'primereact/dialog';
import {useForm} from "react-hook-form";
import filtrosAll from "./filters";

function AdoptionFilter(props) {

    const {register, handleSubmit} = useForm();
    const [visible, setVisible] = useState(false);

    let filtrosAplicados = {
        specie: [],
        sex: [],
        size: []
    };

    function animalFilter(e, filtros, section, index) {

        if (filtrosAll[section][index].filtered) {
            filtrosAll[section][index].filtered = false;
            e.target.src = filtrosAll[section][index].img;
            filtrosAplicados[section] = filtrosAplicados[section].filter(item => item !== filtros);
        } else {
            filtrosAll[section][index].filtered = true;
            e.target.src = filtrosAll[section][index].imgRed;
            filtrosAplicados[section].push(filtros);
        }

    }

    const onSubmit = data => {
        filtrosAplicados["city"] = data.city;
        filtrosAplicados["age"] = data.age;
        props.changeRender(filtrosAplicados);
    };

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Dialog className="c-adoption-dialog" header="" visible={visible} modal={true}
                    onHide={() => setVisible(false)}>
                <div className="c-adoption-filter">
                    <div className="c-adoption-filter__container">
                        <div className="c-adoption-filter__filtro">
                            <p className="c-adoption-filter__mainTitle">Filtros</p>
                            <img onClick={(e) => setVisible(false)} alt="close" className="c-adoption-filter__close"
                                 src={process.env.PUBLIC_URL + "./assets/img/cerrar.png"}/>
                        </div>
                        <div className="c-adoption-filter__ciudad">
                            <p className="c-adoption-filter__title">Ciudad</p>
                            <select ref={register} name="city" id="city" className="c-adoption-filter__select">
                                <option value="Madrid">Madrid</option>
                                <option value="Barcelona">Barcelona</option>
                                <option value="Sevilla">Sevilla</option>
                            </select>
                        </div>
                        <div className="c-adoption-filter__especie">
                            <p className="c-adoption-filter__title">Especie</p>
                            <div className="c-adoption-filter__row">
                                {filtrosAll.specie.map((item, index) =>
                                    <div className="c-adoption-filter__item-container" key={index}>
                                        <div className="c-adoption-filter__square">
                                            <div className="c-adoption-filter__item">

                                                <img onClick={(e) =>
                                                    animalFilter(e, item.name, "specie", index)
                                                }
                                                     className="c-adoption-filter__img c-adoption-filter__img-especie"
                                                     src={item.img} alt={item.name}/>

                                            </div>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className="c-adoption-filter__edad">
                            <p className="c-adoption-filter__title">Edad</p>
                            <select ref={register} name="age" id="data" className="c-adoption-filter__select">
                                <option value="Joven">Joven</option>
                                <option value="Adulto">Adulto</option>
                            </select>
                        </div>
                        <div className="c-adoption-filter__sexo">
                            <p className="c-adoption-filter__title">Sexo</p>
                            <div className="c-adoption-filter__row">
                                {filtrosAll.sex.map((item, index) =>
                                    <div className="c-adoption-filter__item-container" key={index}>
                                        <div className="c-adoption-filter__square">
                                            <div className="c-adoption-filter__item">
                                                <img
                                                    onClick={(e) => animalFilter(e, item.name, "sex", index)}
                                                    className="c-adoption-filter__img c-adoption-filter__img-sexo"
                                                    src={item.img} alt={item.name}/>
                                            </div>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className="c-adoption-filter__tamaño">
                            <p className="c-adoption-filter__title">Tamaño</p>
                            <div className="c-adoption-filter__row">
                                {filtrosAll.size.map((item, index) =>
                                    <div className="c-adoption-filter__item-container" key={index}>
                                        <div className="c-adoption-filter__square">
                                            <div className="c-adoption-filter__item">
                                                <img
                                                    onClick={(e) => animalFilter(e, item.name, "size", index)}
                                                    className="c-adoption-filter__img c-adoption-filter__img-tamaño"
                                                    src={item.img} alt="item.title"/>
                                            </div>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className="c-adoption-filter__button">

                            <button type={"submit"} className="c-adoption-filter__btn">Aplicar</button>

                        </div>
                    </div>
                </div>
            </Dialog>

            <div className="c-adoption" onClick={(e) => setVisible(true)}>
                <img className="c-adoption__img" src={process.env.PUBLIC_URL + "/assets/img/filtros.png"}
                     alt="adoption-img"/>
            </div>

        </form>

    )

}

export default AdoptionFilter;