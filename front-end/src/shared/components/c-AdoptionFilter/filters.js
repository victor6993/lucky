const filtrosAll = {

    specie: [
        {
            img: process.env.PUBLIC_URL + './assets/img/Perro.png',
            imgRed: process.env.PUBLIC_URL + './assets/ICON/Perro_rosa.png',
            filtered: false,
            name: "Perro"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Gato.png',
            imgRed: process.env.PUBLIC_URL + './assets/ICON/Gato_rosa.png',
            filtered: false,
            name: "Gato"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Conejo.png',
            imgRed: process.env.PUBLIC_URL + './assets/ICON/Conejo_rosa.png',
            filtered: false,
            name: "Conejo"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Cobaya.png',
            imgRed: process.env.PUBLIC_URL + './assets/ICON/Cobaya_rosa.png',
            filtered: false,
            name: "Cobaya"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Pequeño_mamifero.png',
            imgRed: process.env.PUBLIC_URL + './assets/ICON/PequeñoMamifero_rosa.png',
            filtered: false,
            name: "Pequeño mamifero"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Huron.png',
            imgRed: process.env.PUBLIC_URL + './assets/ICON/Huron_rosa.png',
            filtered: false,
            name: "Hurón"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Pez.png',
            imgRed: process.env.PUBLIC_URL + './assets/ICON/Pez_rosa.png',
            filtered: false,
            name: "Pez"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Reptil.png',
            imgRed: process.env.PUBLIC_URL + './assets/ICON/Reptil_rosa.png',
            filtered: false,
            name: "Reptil"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Anfibio.png',
            imgRed: process.env.PUBLIC_URL + './assets/ICON/Anfibio_rosa.png',
            filtered: false,
            name: "Anfibio"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Aracnido_o_insecto.png',
            imgRed: process.env.PUBLIC_URL + './assets/ICON/Aracnido_o_insecto_rosa.png',
            filtered: false,
            name: "Aracnido o Insecto"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Ave.png',
            imgRed: process.env.PUBLIC_URL + './assets/ICON/Ave_rosa.png',
            filtered: false,
            name: "Ave"
        }
    ],

    sex: [
        {
            img: process.env.PUBLIC_URL + './assets/img/Hembra.png',
            imgRed: process.env.PUBLIC_URL + './assets/ICON/Hembra_rosa.png',
            filtered: false,
            name: "Hembra"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Macho.png',
            imgRed: process.env.PUBLIC_URL + './assets/ICON/Macho_rosa.png',
            filtered: false,
            name: "Macho"
        }
    ],

    size: [
        {
            img: process.env.PUBLIC_URL + './assets/img/Pequeño.png',
            imgRed: process.env.PUBLIC_URL + './assets/ICON/Pequeño_rosa.png',
            filtered: false,
            name: "Pequeño"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Mediano.png',
            imgRed: process.env.PUBLIC_URL + './assets/ICON/Mediano_rosa.png',
            filtered: false,
            name: "Mediano"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Grande.png',
            imgRed: process.env.PUBLIC_URL + './assets/ICON/Grande_rosa.png',
            filtered: false,
            name: "Grande"
        }
    ]
}

export default filtrosAll;