import React, {useState} from 'react';
import "./AdoptionFilterStatus.scss"
import {Dialog} from 'primereact/dialog';
import {Link} from "react-router-dom";

function AdoptionFilterStatus() {

    const [visible, setVisible] = useState(false);

    const filtrosAdoption = [
        {
            img: process.env.PUBLIC_URL + './assets/img/Completado.png',
            filtered: false
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/En_proceso.png',
            filtered: false
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Rechazado.png',
            filtered: false
        },
    ]

    const images = document.getElementsByClassName("c-filter-status__img");

    function changeBorder() {
        for (let image of images) {
            image.addEventListener("click", function () {
                image.classList.toggle("borderImage-filter-status")
            });
        }
    }

    return (
        <div>
            <div className="c-filter-page" onClick={(e) => setVisible(true)}>
                <img className="c-filter-page__img" src={process.env.PUBLIC_URL + "./assets/img/filtros.png"} alt=""/>
            </div>
            <Dialog header="" visible={visible} style={{}} modal={true} onHide={() => setVisible(false)}>

                <div className="c-filter-status">
                    <div className="c-filter-status__container">
                        <p className="c-filter-status__title">Filtros</p>
                        <div className="row">
                            {filtrosAdoption.map((item, index) =>
                                <div className="col-4" key={index}>
                                    <div className="c-filter-status__item">
                                        <img onClick={(e) => changeBorder()} className="c-filter-status__img"
                                             src={item.img} alt="item.title"/>
                                    </div>
                                </div>
                            )}
                        </div>
                        <div className="c-filter-status__button">
                            <Link to="/adoption-status">
                                <button onClick={(e) => setVisible(false)} className="c-filter-status__btn">Aplicar
                                </button>
                            </Link>
                        </div>
                    </div>
                </div>
            </Dialog>
        </div>
    );
}

export default AdoptionFilterStatus;