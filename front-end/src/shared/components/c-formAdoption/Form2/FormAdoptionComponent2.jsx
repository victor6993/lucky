import React from "react";
import {useForm} from "react-hook-form";
import "./FormAdoptionComponent2.scss"
import {ProgressBar} from "primereact/progressbar";


function FormAdoptionComponent2(props) {

    const {register, handleSubmit} = useForm(0);

    const onSubmit = data => {
        props.handlerForm(2);
        props.addAnimalData(data);
    };


    return (
        <form className="c-form-adoption" onSubmit={handleSubmit(onSubmit)}>
            <p className="c-form-adoption__title">
                Formulario de adopción
            </p>
            <ProgressBar className="c-form-adoption__progress" showValue={false} value={50}/>

            <p className="c-form-adoption__sub-title">
                Sobre las mascotas
            </p>
            <div className="c-form-adoption__container">
                <div className="c-form-adoption__question-container">
                    <label className="c-form-adoption__question">¿Tienes otros animales?</label>
                    <div className="c-form-adoption__yes-no">
                        <div className="c-form-adoption__radio-container">
                            <label className="c-form-adoption__res">Sí</label>
                            <input className="c-form-adoption__input-radio" name="otherAnimals" value={true}
                                   ref={register}
                                   type="radio" required={true}/>
                        </div>
                        <div className="c-form-adoption__radio-container">
                            <label className="c-form-adoption__res">No</label>
                            <input className="c-form-adoption__input-radio" name="otherAnimals" value={false}
                                   ref={register}
                                   type="radio" required={true}/>
                        </div>
                    </div>
                </div>
                <input className="c-form-adoption__input" placeholder="¿Cuales?" name="whichAnimals" ref={register}
                       type="text" required={true}/>
                <input className="c-form-adoption__input" placeholder="¿Se lleva bien con otros animales?"
                       name="friendlyWithOtherAnimals" ref={register} type="text" required={true}/>
            </div>

            <div className="c-form-adoption__container">
                <label className="c-form-adoption__question">¿Por qué has elegido adoptar?</label>
                <input className="c-form-adoption__input" name="whyAdopt" ref={register} type="text" required={true}/>
                <label className="c-form-adoption__question">¿Conoces las necesidades del animal?</label>
                <input className="c-form-adoption__input" name="animalNeeds" ref={register} type="text"
                       required={true}/>
                <label className="c-form-adoption__question">¿Conoces sus gastos?</label>
                <input className="c-form-adoption__input" name="animalExpenses" ref={register} type="text"
                       required={true}/>
                <label className="c-form-adoption__question">¿Conoces su alimentación?</label>
                <input className="c-form-adoption__input" name="animalFeeding" ref={register} type="text"
                       required={true}/>
            </div>

            <button className="c-form-adoption__button">Continuar</button>
        </form>
    );
}

export default FormAdoptionComponent2;