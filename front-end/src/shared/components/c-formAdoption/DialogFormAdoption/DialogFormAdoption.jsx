import React, {useState} from "react";
import {Dialog} from "primereact/dialog";
import {Link} from "react-router-dom";
import "./DialogFormAdoption.scss"

function DialogFormAdoption() {
    const [displayBasic, setDisplayBasic] = useState(false);
    const [position, setPosition] = useState('center');

    const onClick = (stateMethod, position = '') => {
        stateMethod(true);

        if (position) {
            setPosition(position);
        }

    }

    const onHide = (stateMethod) => {
        stateMethod(false);
    }


    return (
        <div className="c-dialog-animal">
            <button onClick={() => onClick(setDisplayBasic)} className="c-dialog-animal__button-send">Enviar</button>

            <Dialog className="c-dialog-animal__content" header="¡Enviado!" visible={displayBasic}
                    style={{width: '50vw'}} onHide={() => onHide(setDisplayBasic)}>
                <Link to="/home"><img className="c-dialog-animal__close"
                     src={process.env.PUBLIC_URL + "./assets/images/cerrar.png"} alt=""/></Link>
                <p className="c-dialog-animal__desc">Hemos enviado tu formulario a la protectora. Si quieres ponerte en contacto con ellos puedes hacerlo vía email o whatsapp.
                </p>
                <p className="c-dialog-animal__desc">Recuerda que la protectora se pondrá en contacto contigo para poder hacer la entrevista personal.
                </p>
                <img className="c-dialog-animal__img"
                     src={process.env.PUBLIC_URL + "/assets/images/undrawPlayfulCatRchv.png"} alt=""/>
            </Dialog>
        </div>
    );
}

export default DialogFormAdoption;