import React from "react";
import {useForm} from "react-hook-form";
import {ProgressBar} from 'primereact/progressbar';
import "./FormAdoptionComponent1.scss"


function FormAdoptionComponent1(props) {

    const {register, handleSubmit} = useForm();

    const onSubmit = data => {
        props.handlerForm(1);
        props.addAnimalData(data);
    }


    return (
        <form className="c-form-adoption" onSubmit={handleSubmit(onSubmit)}>
            <p className="c-form-adoption__title">
                Formulario de adopción
            </p>
            <ProgressBar className="c-form-adoption__progress" showValue={false} value={30}/>
            <p className="c-form-adoption__sub-title">
                Tus datos
            </p>
            <div className="c-form-adoption__container">
                <input autoComplete={false} className="c-form-adoption__input" placeholder="Nombre y apellidos"
                       name="fullName" ref={register} type="text" required={true}/>
                <input autoComplete={false} className="c-form-adoption__input" placeholder="Email" name="email"
                       ref={register} type="email" required={true}/>
                <input autoComplete={false} className="c-form-adoption__input" placeholder="Teléfono" name="phone"
                       ref={register} type="text" required={true}/>
                <input autoComplete={false} className="c-form-adoption__input" placeholder="DNI" name="dni"
                       ref={register} type="text" required={true}/>
            </div>

            <p className="c-form-adoption__sub-title">
                Dirección
            </p>

            <div className="c-form-adoption__container">
                <input autoComplete={false} className="c-form-adoption__input" placeholder="Calle, número, piso"
                       name="address" ref={register} type="text" required={true}/>
                <input autoComplete={false} className="c-form-adoption__input" placeholder="Código postal"
                       name="zipCode" ref={register} type="text" required={true}/>
                <input autoComplete={false} className="c-form-adoption__input" placeholder="Ciudad" name="city"
                       ref={register} type="text" required={true}/>
                <div className="c-form-adoption__policity">
                    <input className="c-form-adoption__input-check" name="" ref={register} type="checkbox"
                           required={true}/>
                    <label className="c-form-adoption__label">Acepto los términos y condiciones de la adopción</label>
                </div>
            </div>

            <button className="c-form-adoption__button">Continuar</button>
        </form>
    );
}

export default FormAdoptionComponent1;