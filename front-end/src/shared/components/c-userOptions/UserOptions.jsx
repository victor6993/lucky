import React, {useContext} from "react";
import "./UserOptions.scss"
import firebase from "firebase"
import UserContext from "../../contexts/UserContext";


function UserOptions() {

    const [user, setUser] = useContext(UserContext);

    return (
        <section className="c-user-options">
            <img className="c-user-options__avatar"
                 src={user ? user.imageUrl : firebase.auth().currentUser ? firebase.auth().currentUser.photoURL : process.env.PUBLIC_URL + "/assets/images/group_blue.png"}
                 alt="user"/>
            <div className="c-user-options__container1">
                <p className="c-user-options__item">
                    <img className="c-user-options__img" src={process.env.PUBLIC_URL + "/assets/images/chica.png"}
                         alt="user"/>
                    <span className="c-user-options__text">Mi perfil</span>
                    <img className="c-user-options__arrow"
                         src={process.env.PUBLIC_URL + "/assets/images/arrow_right_pink.png"} alt="arrow-right"/>
                </p>
                <p className="c-user-options__item">
                    <img className="c-user-options__img"
                         src={process.env.PUBLIC_URL + "/assets/images/localization.png"} alt="localization"/>
                    <span className="c-user-options__text">Direcciones</span>
                    <img className="c-user-options__arrow"
                         src={process.env.PUBLIC_URL + "/assets/images/arrow_right_pink.png"} alt="arrow-right"/>
                </p>
                <p className="c-user-options__item">
                    <img className="c-user-options__img"
                         src={process.env.PUBLIC_URL + "/assets/images/favoritosCopy.png"} alt="favorites"/>
                    <span className="c-user-options__text">Favoritos</span>
                    <img className="c-user-options__arrow"
                         src={process.env.PUBLIC_URL + "/assets/images/arrow_right_pink.png"} alt="arrow-right"/>
                </p>
                <p className="c-user-options__item">
                    <img className="c-user-options__img"
                         src={process.env.PUBLIC_URL + "/assets/images/notificaciones.png"} alt="notifications"/>
                    <span className="c-user-options__text">Notificaciones</span>
                    <img className="c-user-options__arrow"
                         src={process.env.PUBLIC_URL + "/assets/images/arrow_right_pink.png"} alt="arrow-right"/>
                </p>
            </div>

            <div className="c-user-options__container2">
                <p className="c-user-options__item">
                    <img className="c-user-options__img"
                         src={process.env.PUBLIC_URL + "/assets/images/mascota_blue1.png"} alt="pet-blue"/>
                    <span className="c-user-options__text">Estado de la adopcion</span>
                    <img className="c-user-options__arrow"
                         src={process.env.PUBLIC_URL + "/assets/images/arrow_right_pink.png"} alt="arrow-right"/>
                </p>
                <p className="c-user-options__item">
                    <img className="c-user-options__img"
                         src={process.env.PUBLIC_URL + "/assets/images/apadrina_blue.png"} alt="apadrina"/>
                    <span className="c-user-options__text">Apadrinar</span>
                    <img className="c-user-options__arrow"
                         src={process.env.PUBLIC_URL + "/assets/images/arrow_right_pink.png"} alt="arrow-right"/>
                </p>
                <p className="c-user-options__item">
                    <img className="c-user-options__img" src={process.env.PUBLIC_URL + "/assets/images/donar_blue.png"}
                         alt="donate-blue"/>
                    <span className="c-user-options__text">Donar</span>
                    <img className="c-user-options__arrow"
                         src={process.env.PUBLIC_URL + "/assets/images/arrow_right_pink.png"} alt="arrow-right"/>
                </p>
            </div>
        </section>
    );
}

export default UserOptions;