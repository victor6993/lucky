import React from "react";
import "./AdoptionGalleryFilter.scss";
import {Link} from "react-router-dom";

function AdoptionGalleryFilter(props) {

    const url = "/animalprofile/"

    function returnFilter() {
        props.changeRender();
        props.resetFilter();
    }


    return (
        <div className="c-adoption-gallery-filter">
            <div className="p-adoption__input-container c-adoption-gallery-filter__input-container">
                <Link className="c-adoption-gallery__link linkUnderline" to="/adoption">
                    <img onClick={returnFilter} className="c-adoption-gallery-filter__back"
                         src={process.env.PUBLIC_URL + "/assets/images/atras.png"} alt="return"/>
                </Link>
                <input className="p-adoption__input c-adoption-gallery-filter__input" type="text" placeholder="Buscar"/>
                <img className="p-adoption__search c-adoption-gallery-filter__search"
                     src={process.env.PUBLIC_URL + "./assets/Adoption/images/buscar.png"} alt="search"/>
                <img className="c-adoption-gallery-filter__filtros"
                     src={process.env.PUBLIC_URL + "/assets/Adoption/images/filtros.png"} alt="filters"/>

            </div>

            {props.animalsFilter.map((item, index) =>
                <Link to={url + item.id}>
                    <div className="c-adoption-gallery-filter__container" key={index}>
                        <img className="c-adoption-gallery-filter__img-fav"
                             src={process.env.PUBLIC_URL + "./assets/Adoption/images/favoritosRelleno.png"}
                             alt="favorites"/>
                        <img className="c-adoption-gallery-filter__img" src={item.imageUrl} alt="filter-img"/>

                        <div className="c-adoption-gallery-filter__location">
                            <p className="c-adoption-gallery-filter__title">{item.name}</p>
                            <div className="c-adoption-gallery-filter__data">
                                <p className="c-adoption-gallery-filter__city">{item.city}</p>
                                <p className="c-adoption-gallery-filter__city">1.5 km</p>
                            </div>
                        </div>
                    </div>
                </Link>
            )}

        </div>
    );
}

export default AdoptionGalleryFilter;