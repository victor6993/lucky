import React from "react";
import "./AdoptionCarousel.scss"
import {Carousel} from "react-responsive-carousel";


function AdoptionCarousel() {
    const images=[
        {
            path: process.env.PUBLIC_URL + "./assets/Adoption/images/ave.png",
            title: "kiko"
        },
        {
            path: process.env.PUBLIC_URL + "./assets/Adoption/images/perrop.png",
            title: "Apolo",
        },
        {
            path: process.env.PUBLIC_URL + "./assets/Adoption/images/ave.png",
            title: "Dalí"
        }]


    return (

        <Carousel className="c-carousel-adoption">
            {images.map((item,index)=>
                <div className="c-carousel-adoption__container" key={index}>
                    <img className="c-carousel-adoption__img" src={item.path} alt="animal-icon"/>
                    <p className="c-carousel-adoption__title">{item.title}</p>
                </div>
            )}
        </Carousel>

    );
}

export default AdoptionCarousel;