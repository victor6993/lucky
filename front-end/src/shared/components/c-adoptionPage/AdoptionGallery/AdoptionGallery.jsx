import React from "react";
import "./AdoptionGallery.scss";
import {Link} from "react-router-dom";

function AdoptionGallery(props) {
    const url = "/animalprofile/"

    return (
        <div className="c-adoption-gallery">
            {props.animals.map((item, index) =>

                <div className="c-adoption-gallery__container" key={index}>
                    <img className="c-adoption-gallery__img-fav"
                         src={process.env.PUBLIC_URL + "./assets/Adoption/images/favoritosRelleno.png"}/>
                    <Link className="c-adoption-gallery__link" to={url + item.id}>
                        <img className="c-adoption-gallery__img" src={item.imageUrl}/>
                    </Link>
                    <div className="c-adoption-gallery__location">
                        <p className="c-adoption-gallery__title">{item.name}</p>
                        <div className="c-adoption-gallery__data">
                            <p className="c-adoption-gallery__city">{item.city}</p>
                            <p className="c-adoption-gallery__city">1.5km</p>
                        </div>
                    </div>
                </div>
            )}

        </div>
    );
}

export default AdoptionGallery;