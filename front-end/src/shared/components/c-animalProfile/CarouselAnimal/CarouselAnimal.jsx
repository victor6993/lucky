import React from "react";
import {Carousel} from 'react-responsive-carousel';
import "./CarouselAnimal.scss";

function CarouselAnimal(props) {
    return (
        <Carousel className="c-carousel-animal">
            <div className="c-carousel-animal__container">
                <img className="p-animal-profile__img"
                     src={props.animalImage} alt="animal-img"/>
            </div>
        </Carousel>
    );
}

export default CarouselAnimal;