import React from "react";
import {Carousel} from 'react-responsive-carousel';
import "./Carousel.scss"

function CarouselComponent() {

    const images = [
        {
            path: process.env.PUBLIC_URL + "./assets/images/undrawGoodDoggy4Wfq.png",
            title: "Encuentra todo tipo de servicios que tienes cerca de ti"
        },
        {
            path: process.env.PUBLIC_URL + "./assets/images/imagen2.png",
            title: "Adopta desde tu móvil",
            text: "Puedes acceder al perfil de muchos animales que están en adopción y filtrarlos para encontrar el que mejor se adapte a ti"
        },
        {
            path: process.env.PUBLIC_URL + "./assets/images/undrawPetAdoption2Qkw.png",
            title: "Si eres una asociación sube a tus peludos para darles mas difusión"
        }]


    return (
        <Carousel className="c-carousel">
            {images.map((item, index) =>
                <div className="c-carousel__container" key={index}>
                    <img className="c-carousel__img" src={item.path} alt="img"/>
                    <p className="c-carousel__title">{item.title}</p>
                    <p className="c-carousel__text">{item.text}</p>
                </div>
            )}
        </Carousel>
    );
}

export default CarouselComponent;