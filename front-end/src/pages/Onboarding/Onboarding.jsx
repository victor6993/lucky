import React, {useEffect, useState} from "react";
import "./Onboarding.scss"
import CarouselComponent from "../../shared/components/c-onboarding/Carousel/Carousel";
import {Link} from "react-router-dom";


function Onboarding() {

    const [onboardingChoice, setOnboardingChoice] = useState(true)

    useEffect(() => {

        window.addEventListener("click", cb);

    }, []);

    const cb = () => {
        setOnboardingChoice(false)
    }

    window.removeEventListener("click", cb);

    return (
        <div className="p-onboarding">
            {onboardingChoice &&
            <div className="p-onboarding__container">

                <img className="p-onboarding__img"
                     src={process.env.PUBLIC_URL + "./assets/images/group29.png"} alt="Lucky-logo"/>
                <img className="p-onboarding__img" src={process.env.PUBLIC_URL + "./assets/images/group.png"}
                     alt="Lucky"/>

            </div>
            }
            {!onboardingChoice &&
            <div className="p-onboarding__carousel">
                <Link to="/access">
                    <img className="p-onboarding__close"
                         src={process.env.PUBLIC_URL + "./assets/images/cerrar.png"} alt="cerrar"/>
                </Link>
                <CarouselComponent/>
            </div>
            }
        </div>

    )
}

export default Onboarding;