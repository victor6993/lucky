import React from "react";
import MoreOptions from "../../shared/components/c-moreOptions/MoreOptions";

function More() {
    return (
        <div>
            <MoreOptions/>
        </div>
    )
}

export default More;