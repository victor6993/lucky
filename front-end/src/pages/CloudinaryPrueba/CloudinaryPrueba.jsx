import React, {useState} from 'react';
import axios from "axios";
import environment from '../../environment/environment';
import MapServices from "../../shared/components/c-mapsServices/MapServices";

function CloudinaryPrueba() {

    const [image, setImage] = useState('');
    const onChange = e => {
        setImage(e.target.files[0]);
    };

    const onSubmit = () => {
        const formData = new FormData();
        formData.append('file', image);
        formData.append('upload_preset', environment.userPreset);
        // formData.append('upload_preset', environment.associationPreset);
        // formData.append('upload_preset', environment.animalPreset);
        // formData.append('upload_preset', environment.adoptionPreset);
        // formData.append('upload_preset', environment.servicePreset);
        // formData.append('upload_preset', environment.otherPreset);
        axios.post(environment.cloudinaryUrl, formData).then(
            res => {
                console.log(res.data);
                console.log(res.data.secure_url);
                const imageUrl = res.data.secure_url;
                // axios.post('http://localhost:3002/animal-img/1', {
                //     imageUrl
                // });
            }
        )
    };

    const [detailsActive, setDetailsActive] = useState(false);
    const services = [
        {
            name: "Clinica Don Pepe",
        },
        {
            name: "Pelulu",
        },
        {
            name: "Openvet",
        },
        {
            name: "PetDS",
        },
        {
            name: "One Pet Two Pets",
        },
    ]

    return (
        <>
          <div className='container'>
                <h1 className='center red-text'>React Image Upload</h1>
                <div className='file-field input-field'>
                    <div className='btn'>
                        <span>Browse</span>
                        <input type='file' name='image' onChange={onChange}/>
                    </div>
                    <div className='file-path-wrapper'>
                        <input className='file-path validate' type='text'/>
                    </div>
                    <div className='center'>
                        <button onClick={onSubmit} className='btn center'>
                            upload
                        </button>
                        <div onClick={()=> setDetailsActive(true)}
                            style={{width:"20px", height:"20px", borderRadius:"100%", backgroundColor: "rgb(36, 141, 27)", marginTop:"15px"}}></div>
                    </div>
                </div>
            </div>
            {detailsActive && <MapServices services={services}/>}
        </>
    );
}

export default CloudinaryPrueba;