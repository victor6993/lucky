import React, {useContext} from "react";
import './Home.scss';
import HomeCarousel from "../../shared/components/c-home/HomeCarousel/HomeCarousel";
import Novedades from "../../shared/components/c-home/Novedades/Novedades";
import UserContext from "../../shared/contexts/UserContext";
import firebase from "firebase";


function Home() {

    const [user, setUser] = useContext(UserContext);

    return (
        <div className="p-home">
            <img className="p-home__logo" src={process.env.PUBLIC_URL + '/assets/images/logo.png'} alt="logo-img"/>
            <p className="p-home__greetings">{"¡Hola " + (user ? user.name : firebase.auth().currentUser && firebase.auth().currentUser.displayName) + "!"}</p>
            <HomeCarousel/>
            <hr className="p-home__hr"/>
            <Novedades/>
        </div>
    );
}

export default Home;