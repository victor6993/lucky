import React, {useEffect, useState} from "react";
import {TabView, TabPanel} from 'primereact/tabview';
import {Calendar} from 'primereact/calendar';
import {Link} from "react-router-dom";
import {useParams} from "react-router-dom";
import "./AdoptionDetails.scss";
import axios from "axios";
import DialogAdoptionDetails from "../../shared/components/c-adoption-details/DialogAdoptionDetails";
import {useForm} from "react-hook-form";
import environment from "../../environment/environment";

function AdoptionDetails() {

    const [date3, setDate3] = useState(null);
    const [images, setImages] = useState([]);
    const [time, setTime] = useState([]);
    const urlParams = useParams().valueOf();
    const [myAnimal, setMyAnimal] = useState({});
    const {register, handleSubmit} = useForm();


    useEffect(() => {
        axios.get("http://localhost:3002/animal/" + urlParams.id).then(res => {
            setMyAnimal(res.data)
        });
        const hour = 9;
        let time = [];
        for (let i = 0; i <= 10; i++) {
            if ((hour + i) !== 14 || (hour + i) !== 15) {
                time.push(((hour + i) < 10 ? "0" + (hour + i) : (hour + i)) + ":00");
                time.push(((hour + i) < 10 ? "0" + (hour + i) : (hour + i)) + ":30");
            }
        }
        setTime(time);
    }, []);

    function showImg(e, id) {
        setImages([...images, e.target.files[0]]);
        let reader = new FileReader()
        reader.readAsDataURL(e.target.files[0]);
        console.log(reader.result)
        reader.onload = function () {
            let img = document.getElementById(id)
            img.src = reader.result;
            img.style.width = "87px";
            img.style.height = "116px";
        }
    }


    const onSubmit1 = async data => {
        data['imageUrl'] = [];

        for (const image of images) {
            if (image) {
                const formData = new FormData();
                formData.append('file', image);
                formData.append('upload_preset', environment.adoptionPreset);
                try {
                    let result = await axios.post(environment.cloudinaryUrl, formData);
                    data['imageUrl'] = [...data['imageUrl'], result.data.secure_url];
                } catch (err) {
                    console.log(err);
                }
            }
        }

        delete data['time'];
        data['payMethod'] = data['payMethod'][0];
        data['id'] = urlParams.adoptionId;
        axios.put('http://localhost:3002/adoption', {data}).then(res => console.log(res)).catch(err => console.log(err));
    }

    const onSubmit2 = data => {
        delete data['payMethod'];
        data["date"] = date3;
        data['id'] = urlParams.adoptionId;
        axios.put('http://localhost:3002/adoption', {data}).then(res => console.log(res)).catch(err => console.log(err));
    };

    return (
        <section className="p-adoption-details">

            <div>
                <header className="p-animal-profile__header p-adoption-details__header">

                    <p className="p-adoption-details__paragraph">
                        <Link to="/adoption-status">
                            <img className="p-animal-profile__arrow p-adoption-details__arrow"
                                 src={process.env.PUBLIC_URL + "/assets/images/atras.png"} alt="img"/>
                        </Link>
                        Adopcion de {myAnimal.name}
                    </p>
                </header>
                <TabView className="p-animal-profile__tab-view" renderActiveOnly={false}>
                    <TabPanel className="p-animal-profile__options" header="Resumen">
                        <article className="p-animal-profile__articles">
                            <div className="p-animal-profile__characteristics">

                                <div className="c-adoption-status-gallery__container">
                                    <div className="c-adoption-status-gallery__title-status">
                                        <p className="c-adoption-status-gallery__title p-adoption-details__title">Adopción
                                            de {myAnimal.name}</p>
                                    </div>
                                    <div className="c-adoption-status-gallery__location">
                                        <img className="c-adoption-status-gallery__img"
                                             src={myAnimal.imageUrl}/>
                                        <div className="c-adoption-status-gallery__data">
                                            <table className="c-adoption-status-gallery__table">
                                                <tr className="c-adoption-status-gallery__city">
                                                    <td>Nombre</td>
                                                    <td className="c-adoption-status-gallery__value">{myAnimal.name}</td>
                                                </tr>
                                                <tr className="c-adoption-status-gallery__city">
                                                    <td>Ciudad</td>
                                                    <td className="c-adoption-status-gallery__value">{myAnimal.city}</td>
                                                </tr>
                                                <tr className="c-adoption-status-gallery__city">
                                                    <td>Sexo</td>
                                                    <td className="c-adoption-status-gallery__value">{myAnimal.sex}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>


                                <hr className="p-adoption__hr"/>
                                <div className="p-adoption-details__data">
                                    <img className="p-adoption-details__logo"
                                         src={process.env.PUBLIC_URL + "/assets/images/protectoraaso.png"}/>
                                    <div className="p-adoption-details__direccion">
                                        <p className="p-adoption-details__asociacion-name">Asociación Protectora
                                            LARA</p>
                                        <p className="p-adoption-details__street">
                                            <img className="p-adoption-details__street-img"
                                                 src={process.env.PUBLIC_URL + "/assets/images/localization.png"}/>
                                            Calle de Eraso 14, Madrid
                                        </p>
                                    </div>
                                </div>
                                <div>
                                    <img className="p-adoption-details__contact-maps"
                                         src={process.env.PUBLIC_URL + "/assets/images/mapsLara.png"}/>
                                    <p className="p-adoption-details__contact">
                                        Contacta con nosotros
                                        <div className="p-adoption-details__contact-img-container">
                                            <a href={"mailto:asociacion-lara@gmail.com"}>
                                                <img className="p-adoption-details__contact-img"
                                                     src={process.env.PUBLIC_URL + "/assets/images/email.png"}/>
                                            </a>
                                            <img className="p-adoption-details__contact-img"
                                                 src={process.env.PUBLIC_URL + "/assets/images/whatsapp.png"}/>
                                        </div>
                                    </p>
                                </div>
                            </div>

                        </article>
                    </TabPanel>


                    <TabPanel className="p-animal-profile__options" header="Info adicional">
                        <form onSubmit={handleSubmit(onSubmit1)}>
                            <article className="p-animal-profile__articles">
                                <div className="p-animal-profile__characteristics">
                                    <div className="p-animal-profile__history p-adoption-details__subir-img-container">
                                        <p className="p-animal-profile__history-title p-adoption-details__subir-img-title">Subir
                                            imágenes</p>
                                        <p className="p-adoption-details__subir-img-content">Necesitamos que nos subas
                                            algunas fotos de dónde va vivir tu nueva mascota para
                                            poder echarte una mano si necesitas algo más de información</p>
                                        <div className="p-adoption-details__subir-img-input-container">

                                            <input onChange={(e) => {
                                                showImg(e, "imagenCargarda1")
                                            }} id="inputLoad1" className="p-adoption-details__subir-img-input"
                                                   type='file' name='image1'/>
                                            <label className="p-adoption-details__subir-img-label" for="inputLoad1">
                                                <img className="p-adoption-details__img-loaded" id="imagenCargarda1"
                                                     type='file'
                                                     src={process.env.PUBLIC_URL + "/assets/images/cargarimagen.png"}
                                                     alt="subir foto"/>
                                            </label>
                                            <input onChange={(e) => {
                                                showImg(e, "imagenCargarda2")
                                            }} id="inputLoad2" className="p-adoption-details__subir-img-input"
                                                   type='file' name='image2'/>
                                            <label className="p-adoption-details__subir-img-label" for="inputLoad2">
                                                <img className="p-adoption-details__img-loaded" id="imagenCargarda2"
                                                     type='file'
                                                     src={process.env.PUBLIC_URL + "/assets/images/cargarimagen.png"}
                                                     alt="subir foto"/>
                                            </label>
                                            <input onChange={(e) => {
                                                showImg(e, "imagenCargarda3")
                                            }} id="inputLoad3" className="p-adoption-details__subir-img-input"
                                                   type='file' name='image3'/>
                                            <label className="p-adoption-details__subir-img-label" for="inputLoad3">
                                                <img className="p-adoption-details__img-loaded" id="imagenCargarda3"
                                                     type='file'
                                                     src={process.env.PUBLIC_URL + "/assets/images/cargarimagen.png"}
                                                     alt="subir foto"/>
                                            </label>
                                        </div>
                                    </div>

                                </div>
                                <hr className="p-adoption__hr p-adoption-details__hr"/>
                                <p className="p-animal-profile__history-title p-adoption-details__subir-img-title">¿Cómo
                                    quieres pagar las tasas?</p>
                                <p className="p-adoption-details__subir-img-content">Para pagar las tasas de
                                    adopción puedes elegir o pagarlo mediante la app con un pago único o poniéndose
                                    en contacto con la protectora para fraccionar el pago</p>
                                <p className="c-user-options__item p-adoption-details__tasas-item">
                                    <span className="c-user-options__text p-adoption-details__tasas">125€</span>
                                    <span
                                        className="c-user-options__text p-adoption-details__tasas">Desglosar las tasas</span>
                                    <img className="c-user-options__arrow p-adoption-details__arrow-modify"
                                         src={process.env.PUBLIC_URL + "/assets/images/arrowdown.png"} alt=""/>
                                </p>
                                <div className="c-form-adoption__policity p-adoption-details__tipo-de-pago">
                                    <div className="p-adoption-details__tipo-de-pago-item">
                                        <input className="c-form-adoption__input-check p-adoption-details__input-pago"
                                               name="payMethod" value="Pagar a través de la aplicación" type="checkbox"
                                               ref={register}
                                        />
                                        <label className="c-form-adoption__label">Pagar a través de la
                                            aplicación</label>
                                    </div>
                                    <div className="p-adoption-details__tipo-de-pago-item">
                                        {/*<input className="c-form-adoption__input-check p-adoption-details__input-pago"*/}
                                        {/*       name="payMethod" value="Hablar con la protectora" type="radio"*/}
                                        {/*       ref={register}/>*/}
                                        <input className="c-form-adoption__input-check p-adoption-details__input-pago"
                                               name="payMethod" value="Hablar con la protectora" type="checkbox"
                                               ref={register}
                                            // onClick={alredyChecked}
                                            // checked={checked1 ? false : true}/>
                                        />
                                        <label className="c-form-adoption__label">Hablar con la protectora</label>
                                    </div>
                                </div>

                                <button className="p-adoption-details__button">Enviar</button>
                            </article>
                        </form>
                    </TabPanel>


                    <TabPanel className="p-animal-profile__options" header="Adopción">
                        <form onSubmit={handleSubmit(onSubmit2)}>
                            <article className="p-animal-profile__articles">
                                <p className="p-animal-profile__history-title p-adoption-details__subir-img-title">Dirección</p>
                                <div className="p-adoption-details__data-adoption">
                                    <img className="p-adoption-details__logo"
                                         src={process.env.PUBLIC_URL + "/assets/images/protectoraaso.png"}/>
                                    <div className="p-adoption-details__direccion">
                                        <p className="p-adoption-details__asociacion-name">Asociación Protectora
                                            LARA</p>
                                        <p className="p-adoption-details__street">
                                            <img className="p-adoption-details__street-img"
                                                 src={process.env.PUBLIC_URL + "/assets/images/localization.png"}/>
                                            Calle de Eraso 14, Madrid
                                        </p>
                                    </div>
                                </div>
                                <img className="p-adoption-details__contact-maps"
                                     src={process.env.PUBLIC_URL + "/assets/images/mapsLara.png"}/>
                                <hr className="p-adoption__hr p-adoption-details__hr"/>
                                <div>
                                    <p className="p-animal-profile__history-title p-adoption-details__subir-img-title">Día</p>

                                    <p className="c-user-options__item p-adoption-details__tasas-item">
                                        <Calendar dateFormat="DD, dd M" value={date3}
                                                  onChange={(e) => setDate3(e.value)}/>
                                        <img className="p-adoption-details__calendaar-img"
                                             src={process.env.PUBLIC_URL + "/assets/images/calendar.png"}/>
                                    </p>
                                    <p className="p-animal-profile__history-title p-adoption-details__subir-img-title">Hora </p>
                                    <select name="time"
                                            className="c-form-adoption__input p-adoption-details__input-hour p-adoption-details__input-hour--placeholder"
                                            placeholder="¿A qué hora puedes venir?" ref={register} required={true}>
                                        <option disabled value="" selected hidden
                                                className="c-form-adoption__input p-adoption-details__input-hour">¿ A
                                            qué hora puedes venir ?
                                        </option>
                                        {time && time.map((item, index) =>
                                            <option className="c-form-adoption__input p-adoption-details__input-hour"
                                                    key={index} value={item}>{item}</option>
                                        )}
                                    </select>
                                </div>
                                <DialogAdoptionDetails/>
                            </article>
                        </form>
                    </TabPanel>
                </TabView>
            </div>

        </section>
    )
}

export default AdoptionDetails