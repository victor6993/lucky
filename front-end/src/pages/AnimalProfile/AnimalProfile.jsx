import React, {useEffect, useState} from "react";
import {TabView, TabPanel} from 'primereact/tabview';
import "./AnimalProfile.scss"
import {Link} from "react-router-dom";
import CarouselAnimal from "../../shared/components/c-animalProfile/CarouselAnimal/CarouselAnimal";
import DialogAdoption from "../../shared/components/c-animalProfile/Dialog/DialogAdoption";
import {useParams} from "react-router-dom";
import moment from 'moment';
import axios from "axios";


function AnimalProfile() {

    const idAnimal = useParams().valueOf()
    const [animal, setAnimal] = useState({})


    useEffect(() => {
        axios.get("http://localhost:3002/animal/" + idAnimal.id).then(res => setAnimal(res.data));
    }, [idAnimal]);


    return (
        <section className="p-animal-profile">
            {animal && (
                <div>
                    <header className="p-animal-profile__header">
                        <Link to="/adoption">
                            <img className="p-animal-profile__arrow"
                                 src={process.env.PUBLIC_URL + "/assets/images/atras.png"} alt="img"/>
                        </Link>
                        <CarouselAnimal
                            animalImage={animal.imageUrl}
                        />
                    </header>
                    <div className="p-animal-profile__info">
                        <div>
                            <p className="p-animal-profile__name">{animal.name}</p>
                            <p className="p-animal-profile__city">{animal.city}</p>
                        </div>
                        <div className="p-animal-profile__info-animal">
                            <img className="p-animal-profile__icons"
                                 src={process.env.PUBLIC_URL + "/assets/images/favoritos.png"} alt="img"/>
                            <img className="p-animal-profile__icons"
                                 src={process.env.PUBLIC_URL + "/assets/images/compartir.png"} alt="img"/>
                        </div>
                    </div>
                    <TabView className="p-animal-profile__tab-view" renderActiveOnly={false}>
                        <TabPanel className="p-animal-profile__options" header="Datos">
                            <article className="p-animal-profile__articles">
                                <div className="p-animal-profile__characteristics">
                                    <div className="p-animal-profile__data">
                                        <img className="p-animal-profile__data-img"
                                             src={process.env.PUBLIC_URL + "/assets/images/pawprint.png"} alt="img"/>
                                        <span className="p-animal-profile__data-property">Especie</span>
                                        <span className="p-animal-profile__data-value">{animal.specie}</span>
                                    </div>
                                    <div className="p-animal-profile__data">
                                        <img className="p-animal-profile__data-img"
                                             src={process.env.PUBLIC_URL + "/assets/images/pawprint.png"} alt="img"/>
                                        <span className="p-animal-profile__data-property">Fecha de nacimiento</span>
                                        <span
                                            className="p-animal-profile__data-value p-animal-profile__data-value--date-birth">{moment(animal.birth).format('DD - MM - YYYY')}</span>
                                    </div>
                                    <div className="p-animal-profile__data">
                                        <img className="p-animal-profile__data-img"
                                             src={process.env.PUBLIC_URL + "/assets/images/pawprint.png"} alt="img"/>
                                        <span className="p-animal-profile__data-property">Sexo</span>
                                        <span className="p-animal-profile__data-value">{animal.sex}</span>
                                    </div>
                                    <div className="p-animal-profile__data">
                                        <img className="p-animal-profile__data-img"
                                             src={process.env.PUBLIC_URL + "/assets/images/pawprint.png"} alt="img"/>
                                        <span className="p-animal-profile__data-property">Tamaño</span>
                                        <span className="p-animal-profile__data-value">{animal.size}</span>
                                    </div>
                                    <div className="p-animal-profile__data">
                                        <img className="p-animal-profile__data-img"
                                             src={process.env.PUBLIC_URL + "/assets/images/pawprint.png"} alt="img"/>
                                        <span className="p-animal-profile__data-property">Peso</span>
                                        <span className="p-animal-profile__data-value">{animal.weight} Kg</span>
                                    </div>
                                    <div className="p-animal-profile__personality">
                                        <p className="p-animal-profile__personality-title">Personalidad</p>
                                        <div className="p-animal-profile__personality-desc-conatiner">
                                            {animal.personality && animal.personality.map((animal, index) =>
                                                <span key={index}
                                                      className="p-animal-profile__personality-desc">{animal}</span>
                                            )}
                                        </div>
                                    </div>
                                    <div className="p-animal-profile__history">
                                        <p className="p-animal-profile__history-title">Historia</p>
                                        <p className="p-animal-profile__history-desc">{animal.story}</p>
                                    </div>
                                </div>

                            </article>
                        </TabPanel>

                        <TabPanel className="p-animal-profile__options" header="Salud">
                            <article className="p-animal-profile__articles">
                                <div className="p-animal-profile__characteristics">
                                    <div className="p-animal-profile__data">
                                        <img className="p-animal-profile__data-img"
                                             src={process.env.PUBLIC_URL + "/assets/images/pawprint.png"} alt="img"/>
                                        <span className="p-animal-profile__data-property">Vacunado</span>
                                        <span
                                            className="p-animal-profile__data-value">{animal.vaccinated ? "Sí" : "No"}</span>
                                    </div>
                                    <div className="p-animal-profile__data">
                                        <img className="p-animal-profile__data-img"
                                             src={process.env.PUBLIC_URL + "/assets/images/pawprint.png"} alt="img"/>
                                        <span className="p-animal-profile__data-property">Desparasitado</span>
                                        <span
                                            className="p-animal-profile__data-value">{animal.dewormed ? "Sí" : "No"}</span>
                                    </div>
                                    <div className="p-animal-profile__data">
                                        <img className="p-animal-profile__data-img"
                                             src={process.env.PUBLIC_URL + "/assets/images/pawprint.png"} alt="img"/>
                                        <span className="p-animal-profile__data-property">Sano</span>
                                        <span
                                            className="p-animal-profile__data-value">{animal.healthy ? "Sí" : "No"}</span>
                                    </div>
                                    <div className="p-animal-profile__data">
                                        <img className="p-animal-profile__data-img"
                                             src={process.env.PUBLIC_URL + "/assets/images/pawprint.png"} alt="img"/>
                                        <span className="p-animal-profile__data-property">Esterilizado</span>
                                        <span
                                            className="p-animal-profile__data-value">{animal.sterilized ? "Sí" : "No"}</span>
                                    </div>
                                    <div className="p-animal-profile__data">
                                        <img className="p-animal-profile__data-img"
                                             src={process.env.PUBLIC_URL + "/assets/images/pawprint.png"} alt="img"/>
                                        <span className="p-animal-profile__data-property">Identificado</span>
                                        <span
                                            className="p-animal-profile__data-value">{animal.identified ? "Sí" : "No"}</span>
                                    </div>
                                    <div className="p-animal-profile__data">
                                        <img className="p-animal-profile__data-img"
                                             src={process.env.PUBLIC_URL + "/assets/images/pawprint.png"} alt="img"/>
                                        <span className="p-animal-profile__data-property">Microchip</span>
                                        <span
                                            className="p-animal-profile__data-value">{animal.chip ? "Sí" : "No"}</span>
                                    </div>
                                    <div className="p-animal-profile__history">
                                        <p className="p-animal-profile__history-title">Tienes que saber que</p>
                                    </div>
                                </div>
                            </article>
                        </TabPanel>

                        <TabPanel className="p-animal-profile__options" header="Adopción">
                            <article className="p-animal-profile__articles">
                                <div className="p-animal-profile__history">
                                    <p className="p-animal-profile__history-title">Requisitos adopción</p>
                                    <p className="p-animal-profile__history-desc">{animal.requirements}{animal.name}</p>
                                </div>
                                <div className="p-animal-profile__history">
                                    <p className="p-animal-profile__adoption-title">Tasa de adopción
                                        <img
                                            className="p-animal-profile__help"
                                            src={process.env.PUBLIC_URL + "/assets/images/ayuda_rosa.png"} alt="img"/>
                                    </p>
                                    <p className="p-animal-profile__history-desc">{animal.rate}</p>
                                </div>
                                <div className="p-animal-profile__history">
                                    <p className="p-animal-profile__history-title">¿Se envía a otra ciudad?</p>
                                    <p className="p-animal-profile__history-desc">{animal.shipping}</p>
                                </div>
                            </article>
                        </TabPanel>
                    </TabView>
                    <div className="p-animal-profile__buttons">
                        <button className="p-animal-profile__button p-animal-profile__button--white">Apadrinar</button>
                        <DialogAdoption/>
                    </div>
                </div>)}
        </section>
    );
}

export default AnimalProfile