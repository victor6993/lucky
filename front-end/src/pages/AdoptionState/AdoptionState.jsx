import React, {useContext, useEffect, useState} from "react";
import AdoptionStatusGallery
    from "../../shared/components/c-Adoption-Status/AdoptionStatusGallery/AdoptionStatusGallery";
import AdoptionFilterStatus from "../../shared/components/c-Adoption-Status/AdoptionStatusFilter/AdoptionFilterStatus";
import {Link} from "react-router-dom";
import "./AdoptionState.scss"
import axios from "axios";
import UserContext from "../../shared/contexts/UserContext";

function AdoptionState() {

    const [adoptionApplies, setAdoptionApplies] = useState([]);
    const [myAnimals, setMyAnimals] = useState([]);
    const [user, setUser] = useContext(UserContext);

    useEffect(() => {
        let animalFiltered = []
        axios.get("http://localhost:3002/adoption/all").then(res => {
            const filteredAdoptionApplies = res.data.filter(item => item.userId === user._id);
            setAdoptionApplies(filteredAdoptionApplies)
            axios.get("http://localhost:3002/animal/all").then(res => {
                let filteredId = [];
                let animalFilteredStatus = [];
                animalFiltered = filteredAdoptionApplies.map(item => {
                    for (let i = 0; i < res.data.length; i++) {
                        if (res.data[i]._id === item.animalId) {
                            filteredId.push(res.data[i].id)
                            return res.data[i]
                        }
                    }
                    for (let i = 0; i < res.data.length; i++) {
                        if (item.animalId === res.data[i]) {
                            const adoptionData = {
                                adoptionStatus: res.data[i].adoptionStatus,
                                id: res.data[i].id
                            }
                            animalFilteredStatus.push(adoptionData);
                        }
                    }
                });
                for (let i = 0; i < animalFilteredStatus.length; i++) {
                    Object.assign(animalFiltered[i], animalFilteredStatus[i]);
                }
                setMyAnimals(animalFiltered);
            })
        })

    }, []);


    return (
        <div className="p-adoption-state">
            <div className="p-adoption-state__input-container">
                <Link to="/adoption">
                    <img className="p-adoption-state__back"
                         src={process.env.PUBLIC_URL + "/assets/images/atras.png"}/>
                </Link>
                <input className="p-adoption-state__input" type="text" placeholder="Buscar"/>
                <img className="p-adoption-state__search"
                     src={process.env.PUBLIC_URL + "./assets/Adoption/images/buscar.png"}/>
                <AdoptionFilterStatus className="p-adoption-state__filtros"/>
            </div>

            <AdoptionStatusGallery
                myAnimals={myAnimals}
                adoptionApplies={adoptionApplies}
            />
        </div>

    )
}

export default AdoptionState;