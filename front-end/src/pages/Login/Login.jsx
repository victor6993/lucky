import React, {useState} from "react";
import {useForm} from 'react-hook-form';
import './Login.scss'
import LoginDialog from "./LoginDialog/LoginDialog";
import axios from "axios";
import {Redirect} from 'react-router';

function Login(props) {

    const {register, handleSubmit} = useForm();
    const [pass, setPass] = useState("password");
    const [alredyLogged, setAlredyLogged] = useState(false);

    const onSubmit = (data) => {
        axios.post('http://localhost:3002/login', {data}).then(res => {

            localStorage.setItem("lucky-token", JSON.stringify(res.data.token));
            localStorage.setItem("lucky-user", JSON.stringify(res.data.user));

            props.getUser(JSON.parse(localStorage.getItem("lucky-user")), JSON.parse(localStorage.getItem("lucky-token")));

            if (JSON.parse(localStorage.getItem("lucky-token"))) {
                console.log("Estas logado");
                setAlredyLogged(true);
            } else {
                console.log("no estas logado");
            }
        });
    }

    const toggleVisibility = () => {
        setPass(pass === "password" ? "text" : "password");
    }


    return (
        <div className="c-login">
            <div className="c-login__box">
                <img className="c-login__img" src={process.env.PUBLIC_URL + '/assets/images/logo.png'}
                     alt="logo-lucky"/>
                <p className="c-login__text">¡Hola! para continuar, inicia sesión
                    o crea una cuenta</p>
            </div>

            <form className="c-login__form" onSubmit={handleSubmit(onSubmit)}>
                <input className="c-login__input" type="email" name="email" placeholder="Email" ref={register}/>
                <div>
                    <input className="c-login__input c-login__input--password" type={pass} name="password"
                           placeholder="Contraseña"
                           ref={register}/>
                    <img className="c-login__eye"
                         onClick={toggleVisibility}
                         src={process.env.PUBLIC_URL + 'assets/images/ojo.png'} alt="ojo"/>
                </div>
                <p className="c-login__forgotten">¿Has olvidado tu contraseña?</p>
                <div className="c-login__button-box">
                    <button className="c-login__button" type="submit">
                        Iniciar sesión
                    </button>
                    <LoginDialog logInFirebase={props.logInFirebase}/>
                    <button className="c-login__button c-login__button--white" type="button"
                            onClick={() => console.log('clicked')}>
                        Crear cuenta
                    </button>
                </div>
            </form>
            {alredyLogged && <Redirect to="/home"/>}
        </div>
    );
}

export default Login;