import React, {useRef, useState} from "react";
import GoogleMapReact from 'google-map-react';
import useSwr from 'swr';
import useSupercluster from "use-supercluster";
import environment from "../../environment/environment"
import MapsFilter from "../../shared/components/c-MapsFilter/MapsFilter";
import "./Maps.scss";
import MapServices from "../../shared/components/c-mapsServices/MapServices";


function Maps() {

    const [mapHalf, setMapHalf] = useState(false);
    const [renderServices, setRenderServices] = useState(false);

    const fetcher = (...arg) => fetch(...arg).then(response => response.json()).then(res=> {
        const dataFetch=res;
        setAllServices(dataFetch);
        setUserServicesFiltered(dataFetch);
    });
    const Marker = ({children}) => children;

    // 1) map setup
    const [zoom, setZoom] = useState(10);
    const [bounds, setBounds] = useState(null);
    const [userServicesFiltered, setUserServicesFiltered]=useState([]);
    const [allServices, setAllServices]=useState([]);
    const mapRef = useRef();

    // 2) load and format data
    const url = "http://localhost:3002/service/all";
    const {data, error} = useSwr(url, fetcher);

    const points = userServicesFiltered.map(service => ({
        "type": "Feature",
        "properties": {
            "cluster": false,
            "serviceId": service.id,
            "category": service.typeOfService
        },
        "geometry": {"type": "Point", "coordinates": [parseFloat(service.longitude), parseFloat(service.latitude)]}
    }));


    // 3) get clusters
    const {clusters, supercluster} = useSupercluster({
        points,
        bounds,
        zoom,
        options: {radius: 75, maxZoom: 20}
    });
    // console.log(clusters); // Here you can see the groups
    // console.log(userServicesFiltered);

    const filters = (filters) => {
        if (filters.services.length>0) {
            const getFiltered = (array) => {
                return array.filter(item => {
                    for (let i = 0; i < filters.services.length; i++) {
                        if (item.typeOfService === filters.services[i]) {
                            return true;
                        }
                    }
                });
            }
            setUserServicesFiltered(getFiltered(userServicesFiltered));
            setMapHalf(true);
            setRenderServices(true);
        }else{
            setUserServicesFiltered(allServices);
            setMapHalf(false);
            setRenderServices(false);
        }
    }

    // 4) render map
    return (
        <div className="p-maps">

            <header className="p-adoption__input-container p-maps__header">
                <input className="p-maps__input" type="text" placeholder="Buscar"/>
                <img className="p-maps__search"
                     src={process.env.PUBLIC_URL + "./assets/Adoption/images/buscar.png"}/>
                <MapsFilter
                    filters={filters}
                />
            </header>

<div style={mapHalf ? {height: "45vh"} : {height: "76.5vh"}}>
            <GoogleMapReact
                bootstrapURLKeys={{key: environment.googlemapskey}}
                defaultCenter={{lat: 40.416658, lng: -3.703806}}
                defaultZoom={12}
                onChange={({zoom, bounds}) => {
                    setZoom(zoom);
                    setBounds([
                        bounds.nw.lng,
                        bounds.se.lat,
                        bounds.se.lng,
                        bounds.nw.lat
                    ]);
                }}
                yesIWantToUseGoogleMapApiInternals
                onGoogleApiLoaded={({map}) => {
                    mapRef.current = map;
                }}
            >
                {clusters.map(cluster => {
                    const [longitude, latitude] = cluster.geometry.coordinates;
                    const {cluster: isCluster} = cluster.properties;

                    if (isCluster) {
                        return (
                            <Marker
                                key={cluster.id}
                                lat={latitude}
                                lng={longitude}
                            >
                                <div
                                    className="cluster-marker"
                                    style={{
                                        width: `${10 + (cluster.properties.point_count / points.length) * 30}px`,
                                        height: `${10 + (cluster.properties.point_count / points.length) * 30}px`
                                    }}
                                    onClick={() => {
                                        const expansionZoom = Math.min(supercluster.getClusterExpansionZoom(cluster.id), 20);
                                        mapRef.current.setZoom(expansionZoom);
                                        mapRef.current.panTo({lat: latitude, lng: longitude});
                                    }}
                                >
                                    {cluster.properties.point_count}
                                </div>
                            </Marker>
                        );
                    }

                    return (
                        <Marker
                            key={cluster.properties.serviceId}
                            lat={latitude}
                            lng={longitude}
                        >

                            <button
                                className={(cluster.properties && (cluster.properties.category === "Veterinarios") && "place-pin yellow") || ((cluster.properties.category === "Peluquerías") && "place-pin purple") || ((cluster.properties.category === "Educación") && "place-pin blue") || ((cluster.properties.category === "Guardería") && "place-pin pink") || ((cluster.properties.category === "Tienda") && "place-pin orange") || ((cluster.properties.category === "Pet Friendly") && "place-pin green")}/>

                        </Marker>
                    );
                })}
            </GoogleMapReact>
</div>
            {renderServices && <MapServices services={userServicesFiltered}/>}
        </div>
    );
}

export default Maps
