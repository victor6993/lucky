const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

const AssociationSchema = new Schema({
    id: {
        type: Number,
        index: true,
        unique: true,
        require: true
    },
    email: {
        type: String,
        unique: true,
        require: true
    },
    password: {
        type: String,
        require: true
    },
    cif: {
        type: String,
        require: true,
        unique: true
    },
    phone: {
        type: String
    },
    name: {
        type: String,
        require: true
    },
    imageUrl: {
        type: String,
    },
    longitude: {
        type: String
    },
    latitude: {
        type: String
    },
    street: {
        type: String
    },
    zipCode: {
        type: String
    },
    city: {
        type: String
    },
    animals: [{
        type: Schema.Types.ObjectId,
        ref: 'animal'
    }]
});

AssociationSchema.pre('save', async function (next) {

    try {
        if (this.id == undefined) {
            let biggerId = -1;
            const list = await Association.find();
            for (let doc of list) {
                if (doc.id != undefined && biggerId < doc.id) {
                    biggerId = doc.id;
                }
            }
            this.id = ++biggerId;
        }

        if (this.password) this.password = await bcrypt.hash(this.password, 12);

        next();
    } catch (err) {
        console.log(err);
    }
});

const Association = mongoose.model('association', AssociationSchema);
module.exports = Association;

