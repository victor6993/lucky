const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AnimalSchema = new Schema({
    id: {
        type: Number,
        index: true,
        unique: true,
        require: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    association: {
        type: Schema.Types.ObjectId,
        ref: 'association'
    },
    adopted: {
        type: Boolean,
        default: false
    },
    name: {
        type: String,
        require: true
    },
    imageUrl: {
        type: String,
    },
    city: {
        type: String,
        require: true
    },
    specie: {
        type: String,
        enum: ['Perro', 'Gato', 'Conejo', 'Cobaya', 'Pequeño mamifero', 'Hurón', 'Pez', 'Reptil', 'Anfibio', 'Aracnido o Insecto', 'Ave'],
    },
    breed: {
        type: String
    },
    age: {
        type: String,
        enum: ['Joven', 'Adulto'],
        require: true
    },
    sex: {
        type: String,
        enum: ['Macho', 'Hembra'],
    },
    size: {
        type: String,
        enum: ['Pequeño', 'Mediano', 'Grande']
    },
    birth: {
        type: Date,
    },
    weight: {
        type: String,
    },
    personality: {
        type: Array
    },
    story: {
        type: String,
    },
    vaccinated: {
        type: Boolean,
        require: true
    },
    dewormed: {
        type: Boolean,
        require: true
    },
    healthy: {
        type: Boolean,
        require: true
    },
    sterilized: {
        type: Boolean,
        require: true
    },
    identified: {
        type: Boolean,
        require: true
    },
    chip: {
        type: Boolean,
        require: true
    },
    youMayKnow: {
        type: String
    },
    requirements: {
        type: String,
        default: 'No hay requisitos especiales para adoptar a '
    },
    rate: {
        type: String,
        require: true,
    },
    shipping: {
        type: String,
        default: "No se envía a otra ciudad"
    }
});

AnimalSchema.pre('save', async function (next) {

    try {
        if (this.id == undefined) {
            let biggerId = -1;
            const list = await Animal.find();
            for (let doc of list) {
                if (doc.id != undefined && biggerId < doc.id) {
                    biggerId = doc.id;
                }
            }
            this.id = ++biggerId;
        }
        next();
    } catch (err) {
        console.log(err);
    }
});

const Animal = mongoose.model('animal', AnimalSchema);
module.exports = Animal;
