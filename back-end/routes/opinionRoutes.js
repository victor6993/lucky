const express = require('express');
const router = express.Router();
const opinionController = require('../controllers/opinionController');

router.route('/all').get(opinionController.findAllOpinions);
router.route('/:_id').get(opinionController.findOneOpinion)
router.route('/')
    .post(opinionController.addOpinion)
    .put(opinionController.updateOpinion)
    .delete(opinionController.deleteOpinion);

module.exports = router;
