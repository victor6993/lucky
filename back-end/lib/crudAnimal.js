const Animal = require('../models/AnimalSchema');

const findOneAnimalById = ((id) => {
    const animal = Animal.findOne({"id": id}, (err, doc) => {
        console.log(!err ? doc : err);
    });
    return animal;
});

const findAllAnimals = (() => {
    const animals = Animal.find({}, (err, doc) => {
        console.log(!err ? doc : err);
    });
    return animals;
});

const insertOneAnimal = (data) => {
    const newAnimal = new Animal({
        userId: data.userId,
        association: data.association,
        adopted: data.adopted,
        name: data.name,
        imageUrl: data.imageUrl,
        city: data.city,
        specie: data.specie,
        breed: data.breed,
        age: data.age,
        sex: data.sex,
        size: data.size,
        birth: data.birth,
        weight: data.weight,
        personality: data.personality,
        story: data.story,
        vaccinated: data.vaccinated,
        dewormed: data.dewormed,
        healthy: data.healthy,
        sterilized: data.sterilized,
        identified: data.identified,
        chip: data.chip,
        youMayKnow: data.youMayKnow,
        requirements: data.requirements,
        rate: data.rate,
        shipping: data.shipping
    });
    newAnimal.save((err) => {
        console.log(!err ? "Successfuly added a new animal!!" : err);
    })
}

const updateOneAnimal = async (data) => {
    try {
        const animal = await findOneAnimalById(data.id);
        for (let prop in data) {
            data[prop] != undefined ? animal[prop] = data[prop] : null;
        }
        animal.save();
        return animal;
    } catch (err) {
        console.log(err);
        throw err;
    }
};

const deleteOneAnimal = async (id) => {

    try {
        const result = await Animal.remove({"id": id});
        console.log(result);
    } catch (err) {
        console.log(err);
        throw err;
    }
};

module.exports = {
    findOneAnimalById,
    findAllAnimals,
    insertOneAnimal,
    updateOneAnimal,
    deleteOneAnimal
};