const mongoose = require('mongoose');

module.exports = async (req, res, next) => {

    try {
        await  mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});
        console.log('Connected successfully to mongo server');
        next();
    } catch (err) {
        throw err;
    }

}