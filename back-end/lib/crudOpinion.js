const Opinions = require('../models/OpinionSchema');
const Service = require('../lib/crudService');
const User = require('../lib/crudUser');

const findOneOpinionById = ((id) => {
    const opinion = Opinions.findOne({"id": id}, (err, doc) => {
        return !err ? console.log(doc) : err;
    });
    return opinion;
});

const findOneOpinionByObjectId = ((id) => {
    const opinion = Opinions.findOne({"_id": id}, (err, doc) => {
        return !err ? console.log(doc) : err;
    });
    return opinion;
});

const findAllOpinions = (() => {
    const opinions = Opinions.find({}, (err, doc) => {
        return !err ? console.log(doc) : err;
    });
    return opinions;
});

const insertOneOpinion = async (data) => {

    try {
        const user = await User.findOneUserByObjectId(data.userId);
        const newOpinion = await new Opinions({
            userId: data.userId,
            text: data.text,
            valoration: data.valoration,
            userImage: user.imageUrl
        });
        newOpinion.save((err) => {
            !err ? console.log("Successfuly added a new Opinion!!") : console.log(err);
        });
        user.opinions = [...user.opinions, newOpinion._id];
        user.save(err => console.log(!err ? 'usuario guardado con el id opinion' : err));

        const service = await Service.findOneServiceByObjectId(data.serviceId);
        service.opinions = [...service.opinions, newOpinion];
        // service.testimonials = [...service.testimonials, newOpinion._id];
        service.save(err => console.log(!err ? 'servicio guardado con el id de testimonial' : err));

    } catch (err) {
        console.log(err);
    }
}

const updateOneOpinion = async (data) => {

    try {
        const opinion = await findOneOpinionById(data.id);

        for (let prop in data) {
            data[prop] != undefined ? opinion[prop] = data[prop] : null;
        }
        opinion.save();
        return opinion;
    } catch (err) {
        console.log(err);
        throw err;
    }
};

const deleteOneOpinion = async (id) => {

    try {
        const result = await Opinions.remove({"id": id});
        console.log(result);
    } catch (err) {
        console.log(err);
        throw err;
    }
};

module.exports = {
    findOneOpinionById,
    findOneOpinionByObjectId,
    findAllOpinions,
    insertOneOpinion,
    updateOneOpinion,
    deleteOneOpinion
};