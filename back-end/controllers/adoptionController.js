const crudAdoption = require('../lib/crudAdoption');

const findAdoption = async (req, res) => {
    try {
        const adoption = await crudAdoption.findOneAdoptionById(req.body.id);
        res.send(adoption);
    } catch (err) {
        console.log(err);
    }
}

const findAllAdoptions = async (req, res) => {
    try {
        const adoptions = await crudAdoption.findAllAdoptions();
        res.send(adoptions)
    } catch (err) {
        console.log(err);
    }
}

const addAdoption = async (req, res) => {
    try {
        const adoption = await crudAdoption.insertOneAdoption(req.body);
        // const adoption = await crudAdoption.insertOneAdoption(req.body.animalData);
        res.send(adoption);
    } catch (err) {
        console.log(err);
    }
}

const updateAdoption = async (req, res) => {
    try {
        console.log("datos de update adoption", req.body.data);
        const adoption = await crudAdoption.updateOneAdoption(req.body.data);
        res.send(adoption);
    } catch (err) {
        console.log(err);
    }
}

const deleteAdoption = async (req, res) => {
    try {
        const adoption = await crudAdoption.deleteOneAdoption(req.body.id);
        res.send(adoption);
    } catch (err) {
        console.log(err);
    }
}

module.exports = {
    findAdoption,
    findAllAdoptions,
    addAdoption,
    updateAdoption,
    deleteAdoption
};