const crudAnimal = require('../lib/crudAnimal');

const findAnimal = async (req, res) => {
    try {
        const animal = await crudAnimal.findOneAnimalById(req.params.id);
        res.send(animal);
    } catch (err) {
        console.log(err);
    }
}

const findAllAnimals = async (req, res) => {
    try {
        const animals = await crudAnimal.findAllAnimals();
        res.send(animals);
    } catch (err) {
        console.log(err);
    }
}

const addAnimal = async (req, res) => {
    try {
        const animal = await crudAnimal.insertOneAnimal(req.body);
        res.send(animal);
    } catch (err) {
        console.log(err);
    }
}

const updateAnimal = async (req, res) => {
    try {
        const animal = await crudAnimal.updateOneAnimal(req.body);
        // cloudinary.v2.uploader.upload(file, options, callback);
        res.send(animal);
    } catch (err) {
        console.log(err);
    }
}

const deleteAnimal = async (req, res) => {
    try {
        const animal = await crudAnimal.deleteOneAnimal(req.body.id);
        res.send(animal);
    } catch (err) {
        console.log(err);
    }
}

module.exports = {
    findAnimal,
    findAllAnimals,
    addAnimal,
    updateAnimal,
    deleteAnimal
};