const crudServices = require('../lib/crudService');

const findService = async (req, res) => {
    try {
        const service = await crudServices.findOneServiceById(req.params.id);
        res.send(service);
    } catch (err) {
        console.log(err);
    }
}

const findAllServices = async (req, res) => {
    try {
        const service = await crudServices.findAllServices();
        res.send(service);
    } catch (err) {
        console.log(err);
    }
}

const addService = async (req, res) => {
    try {
        const service = await crudServices.insertOneService(req.body);
        res.send(service);
    } catch (err) {
        console.log(err);
    }
}

const updateService = async (req, res) => {
    try {
        const service = await crudServices.updateOneService(req.body);
        res.send(service);
    } catch (err) {
        console.log(err);
    }
}

const deleteService = async (req, res) => {
    try {
        const service = await crudServices.deleteOneService(req.body.id);
        res.send(service);
    } catch (err) {
        console.log(err);
    }
}

module.exports = {
    findService,
    findAllServices,
    addService,
    updateService,
    deleteService
};