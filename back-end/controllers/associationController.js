const crudAssociations = require('../lib/crudAssociation');

const findAllAssociations = async (req, res) => {
    try {
        const users = await crudAssociations.findAllAssociations();
        res.send(users);
    } catch (err) {
        console.log(err);
    }
}

const addAssociation = async (req, res) => {
    try {
        const user = await crudAssociations.insertOneAssociation(req.body);
        res.send(user);
    } catch (err) {
        console.log(err);
    }
}

const updateAssociation = async (req, res) => {
    try {
        const user = await crudAssociations.updateOneAssociation(req.body);
        res.send(user);
    } catch (err) {
        console.log(err);
    }
}

const deleteAssociation = async (req, res) => {
    try {
        const user = await crudAssociations.deleteOneAssociation(req.body.id);
        res.send(user);
    } catch (err) {
        console.log(err);
    }
}

module.exports = {
    findAllAssociations,
    addAssociation,
    updateAssociation,
    deleteAssociation
};