const createError = require('http-errors');
const express = require('express');
require('dotenv').config();
const db = require('./lib/db');
const path = require('path');
const bodyParser = require('body-parser');
const logger = require('morgan');
const cors = require('cors');


const indexRoute = require('./routes/indexRoutes');
const userRouter = require('./routes/userRoutes');
const associationRouter = require('./routes/associationRoutes');
const animalRouter = require('./routes/animalRoutes');
const adoptionRouter = require('./routes/adoptionRoutes');
const serviceRouter = require('./routes/serviceRoutes');
const opinionRouter = require('./routes/opinionRoutes');

const app = express();
app.use(db);
app.use(cors()); //! Add options

app.use(logger('dev'));
app.use(express.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(express.static(path.join(__dirname, 'public')));

app.use(indexRoute);
app.use('/association', associationRouter);
app.use('/user', userRouter);
app.use('/animal', animalRouter);
app.use('/adoption', adoptionRouter);
app.use('/service', serviceRouter);
app.use('/opinion', opinionRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500).send(err);
});

module.exports = app;
